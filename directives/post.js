import template from 'html!./post.html';

export default {
	post () {
		return {
			restrict: 'E',
			scope: {
				info: '='
			},
			link($scope, $elem, attrs) {
				$scope.voteUp = function () {
					$scope.info.votes += 1;
				}
				$scope.voteDown = function () {
					$scope.info.votes -= 1;
				}
			},
			template
		};
	}
};

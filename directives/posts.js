import template from 'html!./posts.html';

export default {
	posts () {
		return {
			scope: false,
			restrict: 'E',
			template
		};
	}
};

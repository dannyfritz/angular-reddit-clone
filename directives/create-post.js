import template from 'html!./create-post.html';

export default {
	createPost () {
		return {
			scope: false,
			restrict: 'E',
			template
		};
	}
};

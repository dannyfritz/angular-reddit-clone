import template from 'html!./header.html';

export default {
	redditHeader () {
		return {
			scope: false,
			restrict: 'E',
			template
		};
	}
};

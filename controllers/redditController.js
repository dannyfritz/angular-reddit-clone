import _ from 'lodash';

export default {
	redditController ($scope) {
		$scope.isCreatePostFormVisible = false;
		$scope.showCreatePostForm = function () {
			$scope.isCreatePostFormVisible = true;
		}
		$scope.hideCreatePostForm = function () {
			$scope.isCreatePostFormVisible = false;
		}

		$scope.posts = [
			{
				id: _.uniqueId(),
				votes: 0,
				title: 'Zion National Park',
				image: 'http://www.visitutah.com/Media/Default/interiorSlideshowImages/Zion%20National%20Park/Zion-Angels-Landing-slide.jpg',
				description: 'A cool NP',
				author: 'Danny Fritz'
			}
		];

		$scope.newPostData = {};
		$scope.createPost = function () {
			$scope.newPostData.id = _.uniqueId();
			$scope.newPostData.votes = 0;
			$scope.posts.push($scope.newPostData);
			$scope.newPostData = {};
			$scope.hideCreatePostForm();
		}
	}
}

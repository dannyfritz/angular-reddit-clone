import Angular from 'angular';
import postSearch from './filters/postSearch.js';
import redditController from './controllers/redditController.js';
import headerDirective from './directives/header.js';
import createPostDirective from './directives/create-post.js';
import postsDirective from './directives/posts.js';
import postDirective from './directives/post.js';

Angular
	.module('redditApp', [])
	.filter(postSearch)
	.controller(redditController)
	.directive(headerDirective)
	.directive(createPostDirective)
	.directive(postsDirective)
	.directive(postDirective)

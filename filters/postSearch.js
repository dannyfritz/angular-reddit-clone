import _ from 'lodash';

export default {
	postSearch () {
		return function(input, searchText) {
			if (_.isEmpty(searchText)) {
				return input;
			}
			return _(input)
				.filter(post => {
					if (_.contains(post.title.toLowerCase(), searchText.toLowerCase())) {
						return true;
					} else if (_.contains(post.description.toLowerCase(), searchText.toLowerCase())) {
						return true;
					}
					return false;
				})
				.value();
		}
	}
};
